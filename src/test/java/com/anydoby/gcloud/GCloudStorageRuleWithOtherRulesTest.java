package com.anydoby.gcloud;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import com.anydoby.gcloud.GCloudStorageRule.StorageRule;
import com.google.cloud.storage.Blob;
import com.google.cloud.storage.StorageOptions;

public class GCloudStorageRuleWithOtherRulesTest {

    @Rule
    public StorageRule rule = GCloudStorageRule.combine(this, ExpectedException.none());

    @Test
    public void testError() {
        rule.rule(ExpectedException.class).expect(IllegalArgumentException.class);
        rule.rule(ExpectedException.class).expectMessage("test");

        rule.cloud().fs("gs://root").file("hello");
        throwError();
    }

    @SuppressWarnings("unused")
    private void throwError() {
        final Iterable<Blob> values = StorageOptions.getDefaultInstance().getService().list("gs://root").getValues();
        for (final Blob blob : values) {
            throw new IllegalArgumentException("test");
        }
    }

}
