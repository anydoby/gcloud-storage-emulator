package com.anydoby.gcloud;

import static com.google.common.collect.Lists.newArrayList;
import static org.junit.Assert.assertEquals;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.channels.Channels;
import java.util.ArrayList;

import org.junit.Rule;
import org.junit.Test;

import com.google.api.gax.paging.Page;
import com.google.cloud.ReadChannel;
import com.google.cloud.storage.Blob;
import com.google.cloud.storage.Storage;
import com.google.cloud.storage.Storage.BlobListOption;
import com.google.cloud.storage.StorageOptions;
import com.google.common.io.ByteStreams;

public class GCloudStorageRuleTest {

    @Rule
    public GCloudStorageRule rule = new GCloudStorageRule();

    @Test
    public void testTheRule_listfiles() {
        rule.fs("gs://hello").file("test1.txt").file("output/test2.txt");

        final Storage service = StorageOptions.getDefaultInstance().getService();
        {
            final Page<Blob> page = service.list("hello");
            final Iterable<Blob> iterateAll = page.iterateAll();
            final ArrayList<Blob> list = newArrayList(iterateAll);
            assertEquals(
                    "[Blob{bucket=hello, name=output, generation=null, size=null, content-type=null, metadata=null}, Blob{bucket=hello, name=test1.txt, generation=null, size=null, content-type=null, metadata=null}]",
                    list.toString());
        }
        {
            final Page<Blob> subdirectory = service.list("hello", BlobListOption.prefix("output"));
            assertEquals("[Blob{bucket=hello, name=output/test2.txt, generation=null, size=null, content-type=null, metadata=null}]",
                    newArrayList(subdirectory.iterateAll()).toString());
        }
    }

    @Test
    public void testTheRule_getFileContents() throws IOException {
        rule.fs("gs://hello").file("test1.txt", new ByteArrayInputStream("hello".getBytes()));

        final Storage service = StorageOptions.getDefaultInstance().getService();
        final Blob next = service.list("hello").getValues().iterator().next();
        assertEquals(5, next.getSize().longValue());
        final byte[] bytes = service.readAllBytes(next.getBlobId());
        assertEquals("hello", new String(bytes));

        final ReadChannel reader = service.reader(next.getBlobId());
        final InputStream newInputStream = Channels.newInputStream(reader);
        assertEquals("hello", new String(ByteStreams.toByteArray(newInputStream)));
    }

}
