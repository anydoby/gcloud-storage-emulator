package com.anydoby.gcloud.gs;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.nio.channels.Channels;
import java.util.ArrayList;

import com.google.api.gax.paging.Page;
import com.google.cloud.storage.Blob;
import com.google.cloud.storage.BlobId;
import com.google.cloud.storage.Storage.BlobListOption;
import com.google.common.collect.Lists;

/**
 * This thing allows creating urls like "gs://mybucket/myfile.txt" and reading from them (of course only mock service will be used).
 */
public class GsURLConnection extends URLConnection {

    private Blob blob;
    private final Handler handler;

    public GsURLConnection(URL url, Handler handler) {
        super(url);
        this.handler = handler;
    }

    @Override
    public void connect() throws IOException {
        final String path = getURL().getPath();
        final String file = getURL().getFile();
        if (path.contains("/")) {
            final String prefix = path.substring(0, path.lastIndexOf('/') - 1);
            final Page<Blob> list = handler.getStorage().list(getURL().getHost(), BlobListOption.prefix(prefix));
            final ArrayList<Blob> files = Lists.newArrayList(list.getValues());
            for (final Blob blob : files) {
                if (blob.getBlobId().getName().equals(file)) {
                    this.blob = blob;
                }
            }
        } else {
            blob = handler.getStorage().get(BlobId.of(getURL().getHost(), file));
        }
    }

    @Override
    public InputStream getInputStream() throws IOException {
        if (blob == null) {
            throw new IllegalStateException("First connect");
        }
        return Channels.newInputStream(handler.getStorage().reader(blob.getBlobId().getBucket(), blob.getBlobId().getName()));
    }

}
