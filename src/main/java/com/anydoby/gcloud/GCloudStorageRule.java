package com.anydoby.gcloud;

import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static java.util.Collections.unmodifiableList;
import static java.util.stream.Collectors.toList;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.security.AccessController;
import java.security.PrivilegedAction;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.TreeMap;

import org.junit.Rule;
import org.junit.rules.TestRule;
import org.junit.runners.model.FrameworkMethod;
import org.junit.runners.model.Statement;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.MockClassLoader;
import org.powermock.core.classloader.annotations.MockPolicy;
import org.powermock.core.classloader.annotations.PrepareEverythingForTest;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.core.classloader.annotations.PrepareOnlyThisForTest;
import org.powermock.core.classloader.annotations.SuppressStaticInitializationFor;
import org.powermock.core.classloader.annotations.UseClassPathAdjuster;
import org.powermock.core.spi.PowerMockPolicy;
import org.powermock.core.transformers.MockTransformer;
import org.powermock.core.transformers.impl.ClassMockTransformer;
import org.powermock.core.transformers.impl.InterfaceMockTransformer;
import org.powermock.core.transformers.impl.TestClassTransformer;
import org.powermock.modules.junit4.common.internal.impl.JUnit4TestMethodChecker;
import org.powermock.modules.junit4.rule.PowerMockRule;
import org.powermock.tests.utils.ArrayMerger;
import org.powermock.tests.utils.IgnorePackagesExtractor;
import org.powermock.tests.utils.TestChunk;
import org.powermock.tests.utils.TestClassesExtractor;
import org.powermock.tests.utils.TestSuiteChunker;
import org.powermock.tests.utils.impl.ArrayMergerImpl;
import org.powermock.tests.utils.impl.MockPolicyInitializerImpl;
import org.powermock.tests.utils.impl.PrepareForTestExtractorImpl;
import org.powermock.tests.utils.impl.StaticConstructorSuppressExtractorImpl;
import org.powermock.tests.utils.impl.TestCaseEntry;
import org.powermock.tests.utils.impl.TestChunkImpl;

import com.anydoby.gcloud.gs.Handler;
import com.google.api.gax.paging.Page;
import com.google.cloud.ReadChannel;
import com.google.cloud.RestorableState;
import com.google.cloud.storage.Blob;
import com.google.cloud.storage.BlobId;
import com.google.cloud.storage.Bucket;
import com.google.cloud.storage.Storage;
import com.google.cloud.storage.Storage.BlobListOption;
import com.google.cloud.storage.StorageOptions;
import com.google.common.collect.Lists;
import com.google.common.io.ByteStreams;

import lombok.SneakyThrows;
import lombok.experimental.Delegate;

/**
 * This rule helps mocking google Storage. This thing is not included into google cloud emulator so everything is mocked.
 *
 * <p>
 * Note: since powermock is extremely unextendable (although there are some hints on interfaces, but every field is either final or static, and classes are
 * package visible), I had to copy-paste a lot to change very little :(
 * <p>
 * Basically we extend from the original {@link PowerMockRule} and reset the testSuiteChunker to a {@link CustomTestSuiteChunker}. This "chunker" is a
 * copy/paste of the original chunker, but adds {@link StorageOptions} as a fixed class to static mock and a selected set of ignore packages (jdk internal
 * ones). Other than that there are no additions.
 * <p>
 * <b>Note: if you use other rules in your test, please see {@link #combine(Object, TestRule...)}</b>
 */
public class GCloudStorageRule extends PowerMockRule {

    protected Storage storage;

    /**
     * Nicely aligns all other rules to work with this one. Do not annotate the rules with {@link Rule}, otherwise nothing will work.
     * <p>
     * Use {@link StorageRule#cloud()} to access the store
     *
     * @param testInstance the test instance, usually <code>this</code> will work fine
     * @param otherRules
     * @return the rule
     */
    public static StorageRule combine(Object testInstance, TestRule... otherRules) {
        return new StorageRule(testInstance, otherRules);
    }

    public static class StorageRule extends MergedRule<GCloudStorageRule> {

        public StorageRule(Object testInstance, TestRule[] otherRules) {
            super(testInstance, new GCloudStorageRule(), otherRules);
        }

        public GCloudStorageRule cloud() {
            return getMethodRule();
        }
    }

    @Override
    public Statement apply(Statement base, FrameworkMethod method, Object target) {
        return super.apply(new Statement() {

            @Override
            public void evaluate() throws Throwable {
                PowerMockito.mockStatic(StorageOptions.class);
                final StorageOptions options = mock(StorageOptions.class);
                when(StorageOptions.getDefaultInstance()).thenReturn(options);
                storage = mock(Storage.class);
                when(storage.getOptions()).thenReturn(options);
                when(options.getService()).thenReturn(storage);
                Handler.setStorage(storage);
                base.evaluate();
            }
        }, method, target);
    }

    @Override
    @SneakyThrows
    protected void init(Object target) {
        final Class<?> testClass = target.getClass();

        set("mockPolicyInitializer", new MockPolicyInitializerImpl(testClass));
        set("previousTargetClass", target.getClass());
        set("testSuiteChunker", new CustomTestSuiteChunker(testClass));
    }

    static class MockClassLoaderFactory {
        private final String[] packagesToIgnore;
        private final Class<?> testClass;
        private final String[] classesToLoadByMockClassloader;
        private final MockTransformer[] extraMockTransformers;

        public MockClassLoaderFactory(Class<?> testClass, String[] classesToLoadByMockClassloader, String[] packagesToIgnore,
                MockTransformer... extraMockTransformers) {
            this.testClass = testClass;
            this.classesToLoadByMockClassloader = classesToLoadByMockClassloader;
            this.packagesToIgnore = packagesToIgnore;
            this.extraMockTransformers = extraMockTransformers;
        }

        public ClassLoader create() {
            final String[] classesToLoadByMockClassloader = makeSureArrayContainsTestClassName(this.classesToLoadByMockClassloader, testClass.getName());

            final ClassLoader mockLoader;
            if (isContextClassLoaderShouldBeUsed(classesToLoadByMockClassloader)) {
                mockLoader = Thread.currentThread().getContextClassLoader();
            } else {
                mockLoader = createMockClassLoader(classesToLoadByMockClassloader);
            }
            return mockLoader;
        }

        protected ClassLoader createMockClassLoader(final String[] classesToLoadByMockClassloader) {

            final List<MockTransformer> mockTransformerChain = getMockTransformers(extraMockTransformers);
            final UseClassPathAdjuster useClassPathAdjuster = testClass.getAnnotation(UseClassPathAdjuster.class);

            final ClassLoader mockLoader = AccessController.doPrivileged(
                    (PrivilegedAction<MockClassLoader>) () -> new MockClassLoader(classesToLoadByMockClassloader, packagesToIgnore, useClassPathAdjuster));

            final MockClassLoader mockClassLoader = (MockClassLoader) mockLoader;
            mockClassLoader.setMockTransformerChain(mockTransformerChain);
            new MockPolicyInitializerImpl(testClass).initialize(mockLoader);
            return mockLoader;
        }

        protected boolean isContextClassLoaderShouldBeUsed(String[] classesToLoadByMockClassloader) {
            return (classesToLoadByMockClassloader == null || classesToLoadByMockClassloader.length == 0) && !hasMockPolicyProvidedClasses(testClass);
        }

        protected List<MockTransformer> getMockTransformers(MockTransformer[] extraMockTransformers) {
            final List<MockTransformer> mockTransformerChain = new ArrayList<>();

            mockTransformerChain.add(new ClassMockTransformer());
            mockTransformerChain.add(new InterfaceMockTransformer());

            Collections.addAll(mockTransformerChain, extraMockTransformers);
            return mockTransformerChain;
        }

        private String[] makeSureArrayContainsTestClassName(String[] arrayOfClassNames, String testClassName) {
            if (null == arrayOfClassNames || 0 == arrayOfClassNames.length) {
                return new String[] { testClassName };

            } else {
                final List<String> modifiedArrayOfClassNames = new ArrayList<>(arrayOfClassNames.length + 1);
                modifiedArrayOfClassNames.add(testClassName);
                for (final String className : arrayOfClassNames) {
                    if (testClassName.equals(className)) {
                        return arrayOfClassNames;
                    } else {
                        modifiedArrayOfClassNames.add(className);
                    }
                }
                return modifiedArrayOfClassNames.toArray(new String[arrayOfClassNames.length + 1]);
            }
        }

        /**
         * @return {@code true} if there are some mock policies that contributes with classes that should be loaded by the mock classloader, {@code false}
         *         otherwise.
         */
        protected boolean hasMockPolicyProvidedClasses(Class<?> testClass) {
            boolean hasMockPolicyProvidedClasses = false;
            if (testClass.isAnnotationPresent(MockPolicy.class)) {
                final MockPolicy annotation = testClass.getAnnotation(MockPolicy.class);
                final Class<? extends PowerMockPolicy>[] value = annotation.value();
                hasMockPolicyProvidedClasses = new MockPolicyInitializerImpl(value).needsInitialization();
            }
            return hasMockPolicyProvidedClasses;
        }
    }

    static class CustomTestSuiteChunker implements TestSuiteChunker {

        protected static final int DEFAULT_TEST_LISTENERS_SIZE = 1;

        protected static final int NOT_INITIALIZED = -1;

        protected static final int INTERNAL_INDEX_NOT_FOUND = NOT_INITIALIZED;

        @PrepareOnlyThisForTest({ StorageOptions.class })
        static class FakeTest {
        }

        /*
         * Maps between a specific class and a map of test methods loaded by a specific mock class loader.
         */
        private final List<TestCaseEntry> internalSuites = new LinkedList<>();
        private final TestClassesExtractor prepareForTestExtractor = new PrepareForTestExtractorImpl() {
            @Override
            protected String[] getClassesToModify(java.lang.reflect.AnnotatedElement element) {
                final String[] inheritedModifications = super.getClassesToModify(element);
                final ArrayList<String> classes = new ArrayList<>();
                if (inheritedModifications != null) {
                    classes.addAll(asList(inheritedModifications));
                }
                classes.addAll(asList(super.getClassesToModify(FakeTest.class)));
                return classes.toArray(new String[0]);
            }
        };
        private final TestClassesExtractor suppressionExtractor = new StaticConstructorSuppressExtractorImpl();
        /*
         * Maps the list of test indexes that is assigned to a specific test suite index.
         */
        protected final LinkedHashMap<Integer, List<Integer>> testAtDelegateMapper = new LinkedHashMap<>();
        protected final Class<?>[] testClasses;
        private final IgnorePackagesExtractor ignorePackagesExtractor = element -> new String[] { "javax.management.*", "sun.*", "javax.*", "com.sun.*",
                "org.xml.*", "org.w3c.*" };
        private final ArrayMerger arrayMerger = new ArrayMergerImpl();
        private int currentTestIndex = NOT_INITIALIZED;

        public CustomTestSuiteChunker(Class<?> testClass) throws Exception {
            testClasses = new Class[] { testClass };
            chunkClass(testClass);
        }

        @Override
        public boolean shouldExecuteTestForMethod(Class<?> testClass, Method potentialTestMethod) {
            return new JUnit4TestMethodChecker(testClass, potentialTestMethod).isTestMethod();
        }

        @Override
        public int getChunkSize() {
            return getTestChunks().size();
        }

        @Override
        public List<TestChunk> getTestChunks() {
            final List<TestChunk> allChunks = new LinkedList<>();
            for (final TestCaseEntry entry : internalSuites) {
                for (final TestChunk chunk : entry.getTestChunks()) {
                    allChunks.add(chunk);
                }
            }
            return allChunks;
        }

        @Override
        public List<TestChunk> getTestChunksEntries(Class<?> testClass) {
            for (final TestCaseEntry entry : internalSuites) {
                if (entry.getTestClass().equals(testClass)) {
                    return entry.getTestChunks();
                }
            }
            return null;
        }

        @Override
        public TestChunk getTestChunk(Method method) {
            for (final TestChunk testChunk : getTestChunks()) {
                if (testChunk.isMethodToBeExecutedByThisClassloader(method)) {
                    return testChunk;
                }
            }
            return null;
        }

        protected void chunkClass(final Class<?> testClass) throws Exception {

            final List<Method> testMethodsForOtherClassLoaders = new ArrayList<>();

            final MockTransformer[] extraMockTransformers = createDefaultExtraMockTransformers(testClass, testMethodsForOtherClassLoaders);

            final String[] ignorePackages = ignorePackagesExtractor.getPackagesToIgnore(testClass);

            final ClassLoader defaultMockLoader = createDefaultMockLoader(testClass, extraMockTransformers, ignorePackages);

            final List<Method> currentClassloaderMethods = new LinkedList<>();
            // Put the first suite in the map of internal suites.
            final TestChunk defaultTestChunk = new TestChunkImpl(defaultMockLoader, currentClassloaderMethods);
            final List<TestChunk> testChunks = new LinkedList<>();
            testChunks.add(defaultTestChunk);
            internalSuites.add(new TestCaseEntry(testClass, testChunks));
            initEntries(internalSuites);

            if (!currentClassloaderMethods.isEmpty()) {
                final List<TestChunk> allTestChunks = internalSuites.get(0).getTestChunks();
                for (final TestChunk chunk : allTestChunks.subList(1, allTestChunks.size())) {
                    for (final Method m : chunk.getTestMethodsToBeExecutedByThisClassloader()) {
                        testMethodsForOtherClassLoaders.add(m);
                    }
                }
            } else if (2 <= internalSuites.size() || 1 == internalSuites.size() && 2 <= internalSuites.get(0).getTestChunks().size()) {
                /*
                 * If we don't have any test that should be executed by the default class loader remove it to avoid duplicate test print outs.
                 */
                internalSuites.get(0).getTestChunks().remove(0);
            }
            // else{ /*Delegation-runner maybe doesn't use test-method annotations!*/ }
        }

        private ClassLoader createDefaultMockLoader(Class<?> testClass, MockTransformer[] extraMockTransformers, String[] ignorePackages) {
            final ClassLoader defaultMockLoader;
            if (testClass.isAnnotationPresent(PrepareEverythingForTest.class)) {
                defaultMockLoader = createNewClassloader(testClass, new String[] { MockClassLoader.MODIFY_ALL_CLASSES }, ignorePackages, extraMockTransformers);
            } else {
                final String[] prepareForTestClasses = prepareForTestExtractor.getTestClasses(testClass);
                final String[] suppressStaticClasses = suppressionExtractor.getTestClasses(testClass);
                defaultMockLoader = createNewClassloader(testClass, arrayMerger.mergeArrays(String.class, prepareForTestClasses, suppressStaticClasses),
                        ignorePackages, extraMockTransformers);
            }
            return defaultMockLoader;
        }

        private ClassLoader createNewClassloader(Class<?> testClass, String[] classesToLoadByMockClassloader, final String[] packagesToIgnore,
                MockTransformer... extraMockTransformers) {
            final MockClassLoaderFactory classLoaderFactory = getMockClassLoaderFactory(testClass, classesToLoadByMockClassloader, packagesToIgnore,
                    extraMockTransformers);
            return classLoaderFactory.create();
        }

        protected MockClassLoaderFactory getMockClassLoaderFactory(Class<?> testClass, String[] preliminaryClassesToLoadByMockClassloader,
                String[] packagesToIgnore, MockTransformer[] extraMockTransformers) {
            return new MockClassLoaderFactory(testClass, preliminaryClassesToLoadByMockClassloader, packagesToIgnore, extraMockTransformers);
        }

        private MockTransformer[] createDefaultExtraMockTransformers(Class<?> testClass, List<Method> testMethodsThatRunOnOtherClassLoaders) {
            if (null == testMethodAnnotation()) {
                return new MockTransformer[0];
            } else {
                return new MockTransformer[] { TestClassTransformer.forTestClass(testClass).removesTestMethodAnnotation(testMethodAnnotation())
                        .fromMethods(testMethodsThatRunOnOtherClassLoaders) };
            }
        }

        protected Class<? extends Annotation> testMethodAnnotation() {
            return null;
        }

        private void initEntries(List<TestCaseEntry> entries) {
            for (final TestCaseEntry testCaseEntry : entries) {
                final Class<?> testClass = testCaseEntry.getTestClass();
                findMethods(testCaseEntry, testClass);
            }
        }

        private void findMethods(TestCaseEntry testCaseEntry, Class<?> testClass) {
            final Method[] allMethods = testClass.getMethods();
            for (final Method method : allMethods) {
                putMethodToChunk(testCaseEntry, testClass, method);
            }
            testClass = testClass.getSuperclass();
            if (!Object.class.equals(testClass)) {
                findMethods(testCaseEntry, testClass);
            }
        }

        private void putMethodToChunk(TestCaseEntry testCaseEntry, Class<?> testClass, Method method) {
            if (shouldExecuteTestForMethod(testClass, method)) {
                currentTestIndex++;
                if (hasChunkAnnotation(method)) {
                    final LinkedList<Method> methodsInThisChunk = new LinkedList<>();
                    methodsInThisChunk.add(method);
                    final String[] staticSuppressionClasses = getStaticSuppressionClasses(testClass, method);
                    final TestClassTransformer[] extraTransformers = null == testMethodAnnotation() ? new TestClassTransformer[0]
                            : new TestClassTransformer[] { TestClassTransformer.forTestClass(testClass).removesTestMethodAnnotation(testMethodAnnotation())
                                    .fromAllMethodsExcept(method) };

                    final ClassLoader mockClassloader;
                    if (method.isAnnotationPresent(PrepareEverythingForTest.class)) {
                        mockClassloader = createNewClassloader(testClass, new String[] { MockClassLoader.MODIFY_ALL_CLASSES },
                                ignorePackagesExtractor.getPackagesToIgnore(testClass), extraTransformers);
                    } else {
                        mockClassloader = createNewClassloader(testClass,
                                arrayMerger.mergeArrays(String.class, prepareForTestExtractor.getTestClasses(method), staticSuppressionClasses),
                                ignorePackagesExtractor.getPackagesToIgnore(testClass), extraTransformers);
                    }
                    final TestChunkImpl chunk = new TestChunkImpl(mockClassloader, methodsInThisChunk);
                    testCaseEntry.getTestChunks().add(chunk);
                    updatedIndexes();
                } else {
                    testCaseEntry.getTestChunks().get(0).getTestMethodsToBeExecutedByThisClassloader().add(method);
                    // currentClassloaderMethods.add(method);
                    final int currentDelegateIndex = internalSuites.size() - 1;
                    /*
                     * Add this test index to the main junit runner delegator.
                     */
                    List<Integer> testList = testAtDelegateMapper.get(currentDelegateIndex);
                    if (testList == null) {
                        testList = new LinkedList<>();
                        testAtDelegateMapper.put(currentDelegateIndex, testList);
                    }

                    testList.add(currentTestIndex);
                }
            }
        }

        private boolean hasChunkAnnotation(Method method) {
            return method.isAnnotationPresent(PrepareForTest.class) || method.isAnnotationPresent(SuppressStaticInitializationFor.class)
                    || method.isAnnotationPresent(PrepareOnlyThisForTest.class) || method.isAnnotationPresent(PrepareEverythingForTest.class);
        }

        private String[] getStaticSuppressionClasses(Class<?> testClass, Method method) {
            final String[] testClasses;
            if (method.isAnnotationPresent(SuppressStaticInitializationFor.class)) {
                testClasses = suppressionExtractor.getTestClasses(method);
            } else {
                testClasses = suppressionExtractor.getTestClasses(testClass);
            }
            return testClasses;
        }

        private void updatedIndexes() {
            final List<Integer> testIndexesForThisClassloader = new LinkedList<>();
            testIndexesForThisClassloader.add(currentTestIndex);
            testAtDelegateMapper.put(internalSuites.size(), testIndexesForThisClassloader);
        }

    }

    private void set(String field, Object value) throws IllegalArgumentException, IllegalAccessException {
        Class<?> clazz = getClass();
        while (clazz != null) {
            try {
                final Field f = clazz.getDeclaredField(field);
                f.setAccessible(true);
                f.set(this, value);
                f.setAccessible(false);
                break;
            } catch (NoSuchFieldException | SecurityException e) {
                clazz = clazz.getSuperclass();
            }
        }
    }

    @SneakyThrows
    public Blob blob(BlobId blobId, byte[] contents) {
        final Class<?> builderClass = Class.forName("com.google.cloud.storage.BlobInfo$BuilderImpl");
        final Constructor<?> bc = builderClass.getDeclaredConstructor(BlobId.class);
        bc.setAccessible(true);
        final Constructor<Blob> constructor = Blob.class.getDeclaredConstructor(Storage.class, builderClass);
        constructor.setAccessible(true);
        final Object builder = bc.newInstance(blobId);
        if (contents != null) {
            final Method method = builder.getClass().getDeclaredMethod("setSize", Long.class);
            method.setAccessible(true);
            method.invoke(builder, Long.valueOf(contents.length));
        }
        final Blob blob = constructor.newInstance(storage, builder);
        when(storage.get(blobId)).thenReturn(blob);
        return blob;
    }

    @SneakyThrows
    public Bucket bucket(String bucketId) {
        final Class<?> builder = Class.forName("com.google.cloud.storage.BucketInfo$BuilderImpl");
        final Constructor<?> bc = builder.getDeclaredConstructor(String.class);
        bc.setAccessible(true);
        final Constructor<Bucket> constructor = Bucket.class.getDeclaredConstructor(Storage.class, builder);
        constructor.setAccessible(true);
        final Object id = bc.newInstance(bucketId);
        final Bucket bucket = constructor.newInstance(storage, id);
        when(storage.get(bucketId)).thenReturn(bucket);
        final String noprotocol = bucketId.replace("gs://", "");
        when(storage.get(noprotocol)).thenReturn(bucket);
        final Page<Blob> emptyPage = pageWith(emptyList());
        when(storage.list(bucketId)).thenReturn(emptyPage);
        when(storage.list(noprotocol)).thenReturn(emptyPage);
        mockLs(noprotocol, "", emptyPage);
        return bucket;
    }

    @SuppressWarnings("unchecked")
    static Page<Blob> pageWith(final List<Blob> listing) {
        final Page<Blob> page = mock(Page.class);
        when(page.iterateAll()).thenReturn(unmodifiableList(listing));
        when(page.getValues()).thenReturn(unmodifiableList(listing));
        return page;
    }

    void mockLs(String bucket, String prefix, Page<Blob> listPage) {
        if (prefix == null) {
            when(storage.list(bucket)).thenReturn(listPage);
            when(storage.list("gs://" + bucket)).thenReturn(listPage);
        } else {
            when(storage.list(bucket, BlobListOption.prefix(prefix))).thenReturn(listPage);
            when(storage.list("gs://" + bucket, BlobListOption.prefix(prefix))).thenReturn(listPage);
        }
    }

    interface Close {
        void close();
    }

    class Node {
        public Node(String name, Bucket bucket) {
            this.name = name;
            this.bucket = bucket;
        }

        public Node(String name, Node node) {
            this(name, node.bucket);
            parent = node;
        }

        Node parent;
        final Bucket bucket;
        TreeMap<String, Node> children = new TreeMap<>();
        final String name;
        byte[] contents;

        public Node node(String name) {
            children.putIfAbsent(name, new Node(name, this));
            return children.get(name);
        }

        public BlobId id() {
            return BlobId.of(bucket.getName(), path());
        }

        public Blob blob() {
            return GCloudStorageRule.this.blob(id(), contents);
        }

        public Node getNode(String child) {
            return children.get(child);
        }

        public String path() {
            final ArrayList<String> path = new ArrayList<>();
            path.add(name);
            Node p = parent;
            while (p != null) {
                path.add(p.name);
                p = p.parent;
            }
            return String.join("/", Lists.reverse(path));
        }
    }

    public class Fs {

        TreeMap<String, Node> children = new TreeMap<>();

        private final Bucket bucket;

        public Fs(String bucket) {
            this.bucket = bucket(bucket);
        }

        public Fs file(String name) {
            fileImpl(name);
            refreshBucketReplies();
            return this;
        }

        private void fileImpl(String name) {
            final String[] paths = name.split("/");
            Node node = node(paths[0]);
            for (int i = 1; i < paths.length; i++) {
                node = node.node(paths[i]);
            }
        }

        private void refreshBucketReplies() {
            final Page<Blob> listPage = listPage(children);
            mockLs(bucket.getName(), null, listPage);
            mockLs(bucket.getName(), "", listPage);
        }

        private Page<Blob> listPage(TreeMap<String, Node> children) {
            for (final Node child : children.values()) {
                if (child.contents == null) {
                    final Page<Blob> childListing = listPage(child.children);
                    mockLs(bucket.getName(), child.path(), childListing);
                }
            }
            final List<Blob> listing = children.values().stream().map(Node::blob).collect(toList());
            final Page<Blob> page = pageWith(listing);
            return page;
        }

        private Node node(String name) {
            children.putIfAbsent(name, new Node(name, bucket));
            return children.get(name);
        }

        @SneakyThrows
        public Fs file(String name, InputStream data) {
            fileImpl(name);
            final Node node = findNode(name);

            final byte[] byteArray = ByteStreams.toByteArray(data);
            node.contents = byteArray;

            when(storage.readAllBytes(node.id())).thenReturn(byteArray);
            when(storage.reader(node.id())).thenAnswer(invocation -> new Channel(Channels.newChannel(new ByteArrayInputStream(byteArray))));
            refreshBucketReplies();
            return this;
        }

        private Node findNode(String path) {
            final String[] parts = path.split("/");
            Node node = children.get(parts[0]);
            for (int i = 1; i < parts.length; i++) {
                node = node.getNode(parts[i]);
            }
            return node;
        }

        class Channel implements ReadChannel, ReadableByteChannel {

            @Delegate(types = { ReadableByteChannel.class }, excludes = Close.class)
            private final ReadableByteChannel channel;

            public Channel(ReadableByteChannel channel) {
                this.channel = channel;
            }

            @Override
            @SneakyThrows
            public void close() {
                this.channel.close();
            }

            @Override
            public RestorableState<ReadChannel> capture() {
                return null;
            }

            @Override
            public void seek(long position) throws IOException {
            }

            @Override
            public void setChunkSize(int chunkSize) {
            }

        }

    }

    /**
     * This bucket will be mocked and will return directories and files if queried by {@link Storage#list(com.google.cloud.storage.Storage.BucketListOption...)}
     * for example, or {@link Storage#list(String, com.google.cloud.storage.Storage.BlobListOption...)}.
     *
     * @param bucket
     * @return ongoing stub for file system mocking.
     */
    @SneakyThrows
    public Fs fs(String bucket) {
        bucket = bucket.replace("gs://", "");
        if (bucket.contains("/")) {
            throw new IllegalArgumentException("Bucket name cannot contain path separators");
        }
        return new Fs(bucket);
    }

    /**
     * Mock storage. You can add expectations to it.
     *
     * @return the storage mock
     */
    public Storage storage() {
        return storage;
    }

}
