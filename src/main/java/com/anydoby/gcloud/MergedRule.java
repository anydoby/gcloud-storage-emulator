package com.anydoby.gcloud;

import java.util.HashMap;
import java.util.Map;

import org.junit.rules.MethodRule;
import org.junit.rules.RuleChain;
import org.junit.rules.TestRule;
import org.junit.runner.Description;
import org.junit.runners.model.FrameworkMethod;
import org.junit.runners.model.Statement;

/**
 * Merges a {@link MethodRule} and a bunch of {@link TestRule}.
 */
public class MergedRule<M extends MethodRule> implements TestRule {

    private RuleChain rule;
    private final M methodRule;
    private final Map<Class<? extends TestRule>, TestRule> rules = new HashMap<>();

    public MergedRule(Object testInstance, M methodRule, TestRule... rules) {
        this.methodRule = methodRule;
        RuleChain outerRule = RuleChain.outerRule((base, description) -> {
            try {
                final FrameworkMethod method = new FrameworkMethod(description.getTestClass().getMethod(description.getMethodName()));
                return methodRule.apply(base, method, testInstance);
            } catch (NoSuchMethodException | SecurityException e) {
                throw new RuntimeException(e);
            }
        });
        for (final TestRule testRule : rules) {
            outerRule = outerRule.around(testRule);
            this.rules.put(testRule.getClass(), testRule);
        }
        this.rule = outerRule;
    }

    @SuppressWarnings("unchecked")
    public <T extends TestRule> T rule(Class<T> id) {
        return (T) rules.get(id);
    }

    public M getMethodRule() {
        return methodRule;
    }

    @Override
    public Statement apply(Statement base, Description description) {
        return rule.apply(base, description);
    }

}
