# Intro

This little utility rule is useful when you want to do some unit testing on the code that uses google Storage API. 

If you have some code doing this:

    Storage service = StorageOptions.getDefaultInstance().getService();
    Page<Blob> page = service.list("mybucket");
    for(Blob file: page.iterateAll()) {
        ReadChannel reader = service.reader(file.getBlobId());
        // do something with data
    }

you can still test it using a special JUnit rule:

	@Rule
    public GCloudStorageRule storage = new GCloudStorageRule();
    
    @Test
    public void testDetermineLatestDirectory() throws Exception {
        // this will create a fake bucket with files (content is optional)
        storage.fs("mybucket").file("somedirectoly/hello.txt", new ByteArrayInputStream("hello".getBytes()));
        
        // then this till work
        Storage service = StorageOptions.getDefaultInstance().getService();
        Page<Blob> page = service.list("mybucket");
        for(Blob file: page.iterateAll()) {
            ReadChannel reader = service.reader(file.getBlobId());
            // do something with data
        }
    }
    
In case your unit tests use other rules, like `ExpectedException`, you will not be able to use them as you were used to do. Instead do the following:

    ExpectedException ex = ExpectedException.none();

    @Rule
    public StorageRule rule = GCloudStorageRule.combine(this, ex);

    @Test
    public void testError() {
        ex.expect(IllegalArgumentException.class);
        ex.expectMessage("test");

        rule.cloud().fs("gs://root").file("hello");
        
        somethingThatThrows();
    }

Have a look at project's tests to get an idea of how to use the rules.

This transparently handles the complexity of JUnit + Powermock which is used under the hood. Happy testing.